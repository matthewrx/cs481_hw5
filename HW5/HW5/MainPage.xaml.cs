﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace HW5
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            // --- Adding Pins 1 ---
            map.MapType = MapType.Street;
            var position = new Position(41.899467, 12.4926164);
            var pinRome = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Rome"
            };
            map.Pins.Add(pinRome);
            // --- Adding Pins 2 ---
            position = new Position(41.801001, 12.5995604);
            var pinCiampino = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "Ciampino"
            };
            map.Pins.Add(pinCiampino);
            // --- Adding Pins 3 ---
            position = new Position(41.9826397, 12.4091872);
            var pinGiustiniana = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "La Giustiniana"
            };
            map.Pins.Add(pinGiustiniana);
            // --- Adding Pins 4 ---
            position = new Position(41.8537322, 12.380161);
            var pinPisana = new Pin
            {
                Type = PinType.Place,
                Position = position,
                Label = "La Pisana"
            };
            map.Pins.Add(pinPisana);
            // --- Default Start Point ---
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pinRome.Position.Latitude, pinRome.Position.Longitude), Distance.FromKilometers(1)));
            // --- Picker View ---
            Dictionary<string, MapType> listView = new Dictionary<string, MapType>
            {
                { "Street", MapType.Street },
                { "Satellite", MapType.Satellite },
                { "Hybrid", MapType.Hybrid },
            };

            Picker pkrView = new Picker
            {
                Title = "Choose a View",
                HeightRequest = 75
            };

            foreach (string ViewMode in listView.Keys)
                pkrView.Items.Add(ViewMode);

            pkrView.SelectedIndexChanged += (sender, args) =>
            {
                if (pkrView.SelectedIndex == -1)
                {
                    map.MapType = MapType.Street;
                }
                else
                {
                    string chooseView = pkrView.Items[pkrView.SelectedIndex];
                    map.MapType = listView[chooseView];
                }
            };
            // --- Picker City ---
            Dictionary<string, Position> listCity = new Dictionary<string, Position>
            {
                { "Rome", pinRome.Position },
                { "Ciampino", pinCiampino.Position },
                { "La Giustiniana", pinGiustiniana.Position },
                { "La Pisana", pinPisana.Position }
            };

            Picker pkrCity = new Picker
            {
                Title = "Choose an italian city",
                HeightRequest = 75
            };

            foreach (string pinCity in listCity.Keys)
                pkrCity.Items.Add(pinCity);

            pkrCity.SelectedIndexChanged += (sender, args) =>
            {
                if (pkrCity.SelectedIndex == -1)
                {
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(pinRome.Position.Latitude, pinRome.Position.Longitude), Distance.FromKilometers(1)));
                }
                else
                {
                    string chooseCity = pkrCity.Items[pkrCity.SelectedIndex];
                    map.MoveToRegion(MapSpan.FromCenterAndRadius(listCity[chooseCity], Distance.FromKilometers(1)));
                }
            };

            // --- Content Page ---
            this.Content = new StackLayout
            {
                Children =
                {
                    map,
                    pkrView,
                    pkrCity
                }
            };
        }
    }
}
